export class NavBarController {
  constructor() {
    'ngInject';

  }

  loadProjects() {
    let query = this.ref.orderByChild("creator").equalTo(this.user.getUid());
    this.projects.data = this.$firebaseArray(query);
    this.projects.data.$loaded()
      .then(angular.bind(this, (list) => {
        this.$log.log(list)
      }))
      .catch(angular.bind(this, (error) => {
        this.$log.log("Error:", error);
      }));
  }
}
// todo - set up nav bar controller.
