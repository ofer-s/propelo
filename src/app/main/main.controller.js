export class MainController {
  constructor(Firebase, FireRef, $log, $state, $window, $document, localStorage, $scope, $firebaseAuth) {
    'ngInject';
    this.FireRef = FireRef;
    this.authObj = $firebaseAuth(FireRef);
    this.$log = $log;
    this.$state = $state;
    this.$window = $window;
    this.$document = $document;
    this.localStorage = localStorage;
    this.authCheck();
    this.user = this.localStorage.getItem('user');
    this.selectNavItem($state.current.name);
    let self = this;
    $scope.$on('$stateChangeStart', (event, toState) => {
      event;
      self.selectNavItem(toState.name);
    });
  }

  selectNavItem(stateName) {
    this.selected = stateName;
  }

  setStageHeight() {
    if(this.logginOut) {
      this.stageHeight = 0
    }
    else {
      let topNavHeight = this.$document.find('#appContainer > .navBar')[0].offsetHeight;
      this.stageHeight = this.$window.innerHeight - topNavHeight;
    }
  }

  logout() {
    this.logginOut = true;
    this.FireRef.unauth();
    this.localStorage.clear();
    this.$state.go('landing');
  }

  authCheck() {
    var authData = this.authObj.$getAuth();
    if (authData) {
      this.$log.log("Logged in as:", authData.uid);
    } else {
      this.$log.error("Logged out");
      this.logout();
    }
  }

  loadProjects() {
    let query = this.FireRef.child('projects').orderByChild("creator").equalTo(this.user.getUid());
    this.projects.data = this.$firebaseArray(query);
    this.projects.data.$loaded()
      .then(angular.bind(this, (list) => {
        this.$log.log(list)
      }))
      .catch(angular.bind(this, (error) => {
        this.$log.log("Error:", error);
      }));
  }
}
