/* global malarkey:false, Firebase:false */

// Import Config files
import { config } from './index.config';
import { mdIconsConfig } from './mdIcons.config';
import { translateConfig } from './translate.config';
import { routerConfig } from './index.route';

// Import Run Block
import { runBlock } from './index.run';

// Import Services
import { LocalStorage } from '../app/services/localStorage';
import { bizLogic } from '../app/services/bizLogic.service';
import { userService } from '../app/services/user.service';

// Import Filters

// Import Controllers
import { MainController } from './main/main.controller';
import { LandingController } from './pages/landing/landing.controller';
import { LoginController } from './pages/landing/login/login.controller';
import { IdeaController } from './pages/idea/idea.controller';
import { HomeController } from './pages/home/home.controller';
import { AddProjectController } from './pages/home/addProject/addProject.controller';

// Import Directives
import { includeReplace } from './utils/includeReplace.directive';

angular.module('propelo', [
    'ngAnimate',
    'ngCookies',
    'ngSanitize',
    'ngMessages',
    'ngAria',
    'ngResource',
    'ui.router',
    'ngMaterial',
    'toastr',
    'ngMdIcons',
    'smoothScroll',
    'ng.deviceDetector',
    'pascalprecht.translate',
    'ngDialog',
    'ui.grid',
    'angularMoment',
    'firebase'])
  .constant('malarkey', malarkey)
  .constant('FireRef', new Firebase("https://propelo.firebaseio.com"))
  .config(config)
  .config(routerConfig)
  .config(mdIconsConfig)
  .config(translateConfig)
  .run(runBlock)
  .factory('localStorage', LocalStorage)
  .factory('bizLogic', bizLogic)
  .factory('user', userService)
  .controller('MainController', MainController)
  .controller('LandingController', LandingController)
  .controller('LoginController', LoginController)
  .controller('IdeaController', IdeaController)
  .controller('HomeController', HomeController)
  .controller('AddProjectController', AddProjectController)
  .directive('includeReplace', includeReplace);
