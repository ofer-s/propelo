/**
 * Created by ofersarid on 14/01/2016.
 */

export function includeReplace() {
  'ngInject';

  let directive = {
    require: 'ngInclude',
    restrict: 'A',
    link: function (scope, el) {
      el.replaceWith(el.children());
    }
  };

  return directive;
}
