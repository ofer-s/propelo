/**
 * Created by ofersarid on 30/01/2016.
 */

export function userService($window) {
  'ngInject';
  function getUid() {
    let ls = $window.localStorage;
    let sessionKey = 'firebase:session::propelo';
    let uid = null;
    if (ls.getItem(sessionKey)){
      uid = angular.fromJson(ls.getItem(sessionKey))['uid'];
    }
    return uid;
  }

  return {
    getUid: getUid
  }
}

