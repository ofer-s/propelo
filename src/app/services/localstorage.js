// An util factory for get/set info from localStorage

export function LocalStorage($window){
  'ngInject';

  let localStorage = $window.localStorage;

  if (!localStorage.getItem('propelo')){
    localStorage.setItem('propelo', '{}');
  }
  let cached = angular.fromJson(localStorage.getItem('propelo'));

  return {
    setItem: (key, value) => {
      cached[key] = value;
      localStorage.setItem('propelo', angular.toJson(cached));
    },

    getItem: (key) => {
      return cached[key];
    },

    removeItem: function (key){
      delete cached[key];
      localStorage.setItem('propelo', angular.toJson(cached));
    },

    clear: () => {
      localStorage.removeItem('propelo')
    }
  }
}

