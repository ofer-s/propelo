/**
 * Created by ofersarid on 21/01/2016.
 */

export function bizLogic() {
  return {
    bizTypes: [{code:'B2B',text:'Business To Business'}, {code:'B2C',text:'Business To Consumer'}, {code:'B2B2C',text:'Business To Business To Consumer'}, {code:'MP',text:'Market Place'}],
    audiences: [{type: 'Consumer',size: 'Single'},{type: 'Micro Business',size: '2-10 employees'},{type: 'Small Business',size: '11-100 employees'},{type: 'Midsize Enterprise',size: '101-1000 employees'},{type: 'Enterprise',size: 'More than 1000 employees'}]
  }
}
