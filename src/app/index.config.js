export function config ($logProvider, toastrConfig, $mdThemingProvider, ngDialogProvider) {
  'ngInject';
  // Enable log
  $logProvider.debugEnabled(true);

  // Set options third-party lib
  toastrConfig.allowHtml = true;
  toastrConfig.timeOut = 3000;
  toastrConfig.positionClass = 'toast-top-right';
  toastrConfig.preventDuplicates = true;
  toastrConfig.progressBar = true;

  // Theme
  $mdThemingProvider.theme('default')
    .accentPalette('green', {
      'default': '600'
    });

  // Dialog
  ngDialogProvider.setDefaults({
    showClose: true,
    closeByDocument: true,
    closeByEscape: true
  });
}
