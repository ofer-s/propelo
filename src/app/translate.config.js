/**
 * Created by ofersarid on 03/01/2016.
 */

export function translateConfig($translateProvider) {
  'ngInject';
  $translateProvider.translations('he', {
    'Google Login': 'כניסה באמצעות גוגל',
    'Login':'כניסה',
    'Login Failed': 'כניסה נכשלה',
    'IDEA': 'רעיון',
    'Audience': 'קהל יעד',
    'Users': 'משתמשים',
    'Customers': 'לקוחות',
    'Small Businesses': 'עסקים קטנים',
    'Medium Businesses': 'עסקים בינוניים',
    'Large Businesses': 'עסקים גדולים',
    'Project Created': 'פרוייקט נוצר'
  });

  $translateProvider.preferredLanguage('en');
}
