export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('landing', {
      url: '/',
      templateUrl: 'app/pages/landing/landing.html',
      controller: 'LandingController as landing',
      bindToController: {}
    })
    .state('app', {
      url: '/app',
      templateUrl: 'app/main/main.html',
      controller: 'MainController as main',
      bindToController: {
        selected: '='
      }
    })
    .state('app.home', {
      url: '/home',
      templateUrl: 'app/pages/home/home.html',
      controller: 'HomeController as home',
      bindToController: {}
    })
    .state('app.idea', {
      url: '/idea',
      templateUrl: 'app/pages/idea/idea.html',
      controller: 'IdeaController as idea',
      bindToController: {}
    });

  $urlRouterProvider.otherwise('/app/home');
}
