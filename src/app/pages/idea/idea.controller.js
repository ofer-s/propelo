/**
 * Created by ofersarid on 17/01/2016.
 */

export class IdeaController {
  constructor() {
    'ngInject';
    this.init();
  }

  init() {
    // Init Business Type
    this.bizType = 'Business To Business';
    this.bizTypes = ['Business To Business', 'Business To Consumer', 'Business To Business To Consumer', 'Market Place'];

    // Init Audience
    this.audience = 'Consumer';
    this.audienceDefinitions = [
      {
        type: 'Consumer',
        size: 'Single'
      },
      {
        type: 'Micro Business',
        size: '2-10 employees'
      },
      {
        type: 'Small Business',
        size: '11-100 employees'
      },
      {
        type: 'Midsize Enterprise',
        size: '101-1000 employees'
      },
      {
        type: 'Enterprise',
        size: 'More than 1000 employees'
      }
    ]
  }
}
