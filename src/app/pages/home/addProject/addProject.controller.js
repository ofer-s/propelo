/**
 * Created by ofersarid on 21/01/2016.
 */

export class AddProjectController {
  constructor(bizLogic, FireRef, localStorage, $log, $firebaseObject, $mdToast, $scope) {
    'ngInject';
    this.$log = $log;
    this.$scope = $scope;
    this.$mdToast = $mdToast;
    this.localStorage = localStorage;
    let ref = FireRef.child('projects');
    this.projects = $firebaseObject(ref);
    this.bizLogic = bizLogic;
    this.bizCode = 'B2B';
    this.name = '';
  }

  start() {
    let now = new Date().getTime();
    let id = this.localStorage.getItem('uid') +'-'+ (now);
    this.projects[id] = {
      name: this.name,
      biz_type: this.bizCode,
      created_at: now,
      updated_at: now,
      creator: this.localStorage.getItem('uid'),
      creator_name: this.localStorage.getItem('user').given_name
    };
    let self = this;
    this.projects.$save().then(function() {
      self.$mdToast.show({
        template: '<md-toast><ng-md-icon class="icon" icon="check"></ng-md-icon><span>{{"Project Created" | translate}}</span> </md-toast>',
        hideDelay: 3000,
        position: 'bottom right'
      });
      self.$scope.closeThisDialog();
    }, function(error) {
      self.$log.log("Error:", error);
    });
  }
}
