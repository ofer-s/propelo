/**
 * Created by ofersarid on 17/01/2016.
 */

export class HomeController {
  constructor (ngDialog, FireRef, $firebaseArray, $log, user) {
    'ngInject';
    this.ref = FireRef.child('projects');
    this.ngDialog = ngDialog;
    this.user = user;
    this.$log = $log;
    this.$firebaseArray = $firebaseArray;
    this.projects = {};
    this.initGrid();
    this.loadProjects();
  }

  initGrid() {
    this.projects.columnDefs = [{name:'name'}, {name: 'type'}, {name: 'creator'}]
  }

  loadProjects() {
    let query = this.ref.orderByChild("creator").equalTo(this.user.getUid());
    this.projects.data = this.$firebaseArray(query);
    this.projects.data.$loaded()
      .then(angular.bind(this, (list) => {
        this.$log.log(list)
      }))
      .catch(angular.bind(this, (error) => {
        this.$log.log("Error:", error);
      }));
  }

  startNewProject() {
    this.ngDialog.open({
      template: 'app/pages/home/addProject/addProject.html',
      controller: 'AddProjectController as addProj',
      className: 'ngdialog ngdialog-theme-default addProjectDialog'
    });
  }
}
