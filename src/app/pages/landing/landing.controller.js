/**
 * Created by ofersarid on 04/01/2016.
 */

export class LandingController {
  constructor(Firebase, $window, $log, $state, ngDialog) {
    'ngInject'
    this.ref = new Firebase("https://propelo.firebaseio.com");
    this.$log = $log;
    this.$state = $state;
    this.localStorage = $window.localStorage;
    this.ngDialog = ngDialog;
    this.loginPopUp = null;
  }

  openLogin() {
    if(this.loginPopUp) return;
    let self = this;
    this.loginPopUp = this.ngDialog.open({
      template: 'app/pages/landing/login/login.html',
      className: 'ngdialog-theme-default login',
      controller: 'LoginController as login',
      preCloseCallback: () => {
        self.loginPopUp = null;
      }
    });
  }

  login() {
    // login with Google
    let self = this;
    this.ref.authWithOAuthPopup("google", function (error, authData) {
      if (error) {
        self.$log.log("Login Failed!", error);
        self.$state.go('landing');
      } else {
        self.$log.log("Authenticated successfully with payload:", authData);
        self.localStorage.saveItem('user', {
          email: authData.google.email,
          given_name: authData.google.cachedUserProfile.given_name,
          family_name: authData.google.cachedUserProfile.family_name,
          picture: authData.google.cachedUserProfile.picture
        });
        self.ref.child("users").child(authData.uid).set(self.localStorage.getItem('user'));
        self.$state.go('  home');
      }
    }, {
      scope: "email"
    });
  }
}
