/**
 * Created by ofersarid on 06/01/2016.
 */

export class LoginController {
  constructor($log, $state, $mdToast, $scope, localStorage, $firebaseAuth, FireRef) {
    'ngInject'
    this.FireRef = FireRef;
    this.authObj = $firebaseAuth(this.FireRef);
    this.$log = $log;
    this.$mdToast = $mdToast;
    this.$scope = $scope;
    this.$state = $state;
    this.localStorage = localStorage;
    'ngInject';
  }

  login() {
    // login with Google
    let self = this;
    this.authObj.$authWithOAuthPopup("google", {
      scope: "email"
    }).then(function(authData) {
      self.$log.log("Logged in as:", authData.uid);
      self.localStorage.setItem('user', {
        email: authData.google.email,
        given_name: authData.google.cachedUserProfile.given_name,
        family_name: authData.google.cachedUserProfile.family_name,
        picture: authData.google.cachedUserProfile.picture
      });
      self.FireRef.child("users").child(authData.uid).set(self.localStorage.getItem('user'));
      self.localStorage.setItem('uid', authData.uid);
      self.$state.go('app');
      self.$scope.closeThisDialog();
    }).catch(function(error) {
      self.$log.error("Authentication failed:", error);
      self.$mdToast.show({
        template: '<md-toast class="error"><ng-md-icon class="icon" icon="do_not_disturb"></ng-md-icon><span>{{"Login Failed" | translate}}</span> </md-toast>',
        hideDelay: 4000,
        position: 'bottom right'
      });
      self.$state.go('landing');
    });
  }
}
